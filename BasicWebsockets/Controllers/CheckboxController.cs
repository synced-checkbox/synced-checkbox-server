﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BasicWebsockets.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BasicWebsockets.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CheckboxController : ControllerBase
    {
        private CheckboxData _checkboxData;
        private BackgroundSocketProcessor _backgroundSocketProcessor;

        public CheckboxController(CheckboxData checkboxData, BackgroundSocketProcessor backgroundSocketProcessor)
        {
            _checkboxData = checkboxData;
            _backgroundSocketProcessor = backgroundSocketProcessor;
        }

        [HttpGet]
        public ActionResult Get()
        {

            return Ok(_checkboxData.Status);
        }

        [HttpPost("toggle")]
        public ActionResult Toggle()
        {
            _checkboxData.Toggle();

            _backgroundSocketProcessor.SendToAllActiveClients($"{_checkboxData.Status},{_checkboxData.StateNumber}");

            return Ok(_checkboxData.CurrentState);
        }
    }
}
