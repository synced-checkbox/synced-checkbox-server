﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BasicWebsockets.Controllers
{
    public class CheckboxState
    {
        public long StateNumber { get; set; }
        public bool Status { get; set; }
    }
    public class CheckboxData
    {
        private long _status = 0;
        private long _stateNumber = 1;
        public bool Status
        {
            get
            {
                /* Interlocked.Read() is only available for int64,
                 * so we have to represent the bool as a long with 0's and 1's
                 */
                return Interlocked.Read(ref _status) == 1;
            }
            set
            {
                Interlocked.Exchange(ref _status, Convert.ToInt64(value));
            }
        }

        public long StateNumber
        {
            get
            {
                return Interlocked.Read(ref _stateNumber);
            }
        }

        public CheckboxState CurrentState
        {
            get
            {
                return new CheckboxState()
                {
                    StateNumber = this.StateNumber,
                    Status = this.Status
                };
            }
        }



        public void Toggle()
        {
            Interlocked.Increment(ref _stateNumber);
            if (Interlocked.Read(ref _status) == 0)
            {
                Status = true;
                return;
            };
            Status = false;
        }
    }
}
