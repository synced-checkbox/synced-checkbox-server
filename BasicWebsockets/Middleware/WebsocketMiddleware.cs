﻿using BasicWebsockets.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;

namespace BasicWebsockets.Middleware
{
    public class WebsocketMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public WebsocketMiddleware(RequestDelegate next, ILoggerFactory logFactory)
        {
            _next = next;

            _logger = logFactory.CreateLogger("MyMiddleware");
        }

        public async Task Invoke(HttpContext httpContext, BackgroundSocketProcessor backgroundSocketProcessor)
        {
            if (httpContext.Request.Path == "/ws")
            {
                if (httpContext.WebSockets.IsWebSocketRequest)
                {
                    using (WebSocket webSocket = await httpContext.WebSockets.AcceptWebSocketAsync())
                    {
                        var socketFinishedTcs = new TaskCompletionSource<object>();

                        backgroundSocketProcessor.AddSocket(webSocket, socketFinishedTcs);

                        await socketFinishedTcs.Task;
                    }
                }
                else
                {
                    httpContext.Response.StatusCode = 400;
                }
            }
            else
            {
                await _next(httpContext);
            }
        }
    }
}
