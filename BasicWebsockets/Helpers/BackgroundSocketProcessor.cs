﻿using BasicWebsockets.Controllers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BasicWebsockets.Helpers
{
    public class BackgroundSocketProcessor
    {
        public CheckboxData _checkboxData;
        public BackgroundSocketProcessor(CheckboxData checkboxData)
        {
            _checkboxData = checkboxData;
            var Timer = new Timer(async state =>
            {
                await RemoveInactiveSockets();
            }, null, 0, 30000);
        }

        class SocketData
        {
            public TaskCompletionSource<object> CompletionSource { get; set; }
            public WebSocket Socket { get; set; }
            public CancellationTokenSource CancellationTokenSource { get; set; }
            public DateTime LastPing { get; set; }
        }

        private ConcurrentDictionary<string, SocketData> _activeSockets = new ConcurrentDictionary<string, SocketData>();

        public void AddSocket(WebSocket socket, TaskCompletionSource<object> completionSource)
        {
            var id = Guid.NewGuid().ToString();

            var cancellationTokenSource = new CancellationTokenSource();

            var socketData = new SocketData()
            {
                CompletionSource = completionSource,
                Socket = socket,
                CancellationTokenSource = cancellationTokenSource,
                LastPing = DateTime.UtcNow
            };

            var receiveTask = new Task(async () =>
            {
                try
                {
                    var buffer = new byte[8];
                    while (socket.State == WebSocketState.Open)
                    {
                        var incomingData = await socket.ReceiveAsync(buffer, cancellationTokenSource.Token);
                        var data = Encoding.UTF8.GetString(buffer, 0, incomingData.Count);

                        if (data == "ping")
                        {
                            socketData.LastPing = DateTime.UtcNow;
                        }
                    }
                }
                catch (OperationCanceledException) { };
            }, cancellationTokenSource.Token);

            _activeSockets.TryAdd(id, socketData);
            receiveTask.Start();

            socket.SendAsync(Encoding.UTF8.GetBytes($"{_checkboxData.Status},{_checkboxData.StateNumber},{id}"), WebSocketMessageType.Text, true, new CancellationToken());
        }

        public void SendToAllActiveClients(string message)
        {
            var token = new CancellationToken();

            foreach (var socketData in _activeSockets.Values)
            {
                var socket = socketData.Socket;
                if (socket.State == WebSocketState.Open)
                {
                    socket.SendAsync(Encoding.UTF8.GetBytes(message), WebSocketMessageType.Text, true, token);
                }
            }
        }

        public async Task<bool> RemoveClient(string id)
        {

            SocketData socketData;

            var success = _activeSockets.TryRemove(id, out socketData);

            if (!success)
            {
                return success;
            }

            var socket = socketData.Socket;

            socketData.CancellationTokenSource.Cancel();

            if (socket.State != WebSocketState.Closed)
            {
                await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Socket closed by server", new CancellationToken() );
            }

            socket.Dispose();

            return true;
        }

        private async Task RemoveInactiveSockets()
        {
            foreach(var activeSocket in _activeSockets)
            {
                if (activeSocket.Value.LastPing < DateTime.UtcNow.AddSeconds(-30) || activeSocket.Value.Socket.State == WebSocketState.Closed)
                {
                    await RemoveClient(activeSocket.Key);
                }
            }
        }
    }
}
